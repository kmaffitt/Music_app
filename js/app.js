// $(document).foundation()

'use strict'

var events


var Events = function(JSON) {
  this.JSON = JSON
  this.events = new Array()

  this.populate()
  console.log(this.events)
  this.write()
  // this.writeJSON()

  //
  this.drawMarkers()
}

Events.prototype.drawMarkers = function(){
  console.log('drawing map markers');

  var addresses = new Array()

  for(event in this.events){
    geocodeAddress(this.events[event]);
  }
}

Events.prototype.populate = function() {
  console.log('populating events list...')
  for (event in this.JSON) {
    this.events.push(new Event(this.JSON[event]))
  }
}

Events.prototype.write = function() {
  console.log('writing single event to page...')
  var eventsId = document.getElementById('events')

  var innerId
  for (event in this.events) {
    eventsId.innerHTML += '<div class=\'callout\' id=' + event + '>'
    innerId = document.getElementById(event)

    innerId.innerHTML += '<h3>' + this.events[event].title + '</h3>\
                          ' + '<h5>Date: ' + this.events[event].startDate + ' AM - '+ this.events[event].endDate.substring(this.events[event].endDate.length - 8) + ' AM</h5>\
                          ' + '<h5>Address: ' + this.events[event].address + '</h5>\
                          ' + '<h5>Cost: ' + this.events[event].cost + '</h5>\
                          <p> ' + this.events[event].description + '</p>\
                          <img src=\'' + this.events[event].image  + '\'</img>'
    eventsId.innerHTML += '</div>'

  }
}

Events.prototype.writeJSON = function() {
  console.log('writing JSON data to page...')
  var eventsId = document.getElementById('json-events')

  eventsId.innerHTML += '<div class=\'callout\' id=\'json-inner\'>'

  var innerId = document.getElementById('json-inner')
  innerId.innerHTML += '<h2>JSON Data</h2>'
  for (event in this.events) {
    innerId.innerHTML += JSON.stringify(this.events[event]) + '<br>'
  }
  eventsId.innerHTML += '</div>'
}

var Event = function(event) {
  this.id = event.id
  this.title = event.title
  this.description = event.description
  this.url = event.url
  this.image = event.image.url
  this.categories = event.categories
  this.allDay = event.all_day
  this.cost = event.cost
  this.costDetails = event.cost_details
  this.date = event.date
  this.venue = event.venue
  this.address = event.venue.address + ' ' + event.venue.city + ' ' + event.venue.country + ' ' + event.venue.city + ', ' + event.venue.state + ' ' + event.venue.zip
  this.startDate = event.start_date
  this.endDate = event.end_date
  this.utcStartDate = event.utc_start_date
  this.utcEndDate = event.utc_end_date
}

function getEvents() {
  console.log('getting events...')
  // get current date
  var currentDate = new Date()

  // format current date
  var currentDateFormatted = currentDate.getFullYear() + '-' + (currentDate.getMonth()+1) + '-' + currentDate.getDate() + ' ' + currentDate.getHours() + ':' + currentDate.getMinutes() + ':' + currentDate.getSeconds()
  console.log('current date: ' + currentDateFormatted)

  // encode the formatted current date into a query
  var dateQuery = encodeURIComponent(currentDateFormatted)

  var xhr = new XMLHttpRequest();
  xhr.open('GET', 'http://desleefinearts.org/wp-json/tribe/events/v1/events?per_page=30&start_date=' + dateQuery, true);
  // console.log('http://desleefinearts.org/wp-json/tribe/events/v1/events?per_page=30&start_date=' + dateQuery)
  xhr.send();

  xhr.onload = function () {
    // once the response is complete and the request was successful, do something with the response
    if (xhr.readyState === xhr.DONE && xhr.status === 200) {
      events = new Events(JSON.parse(xhr.responseText).events)
    }
  }
  

}
 

