//array of unique addresses
var addresses
var geocoder

//creates map canvas centered on STL, 
function initMap() {
  var stl = {lat: 38.627, lng: -90.199};
  ourMap = new google.maps.Map(document.getElementById('map'), {
    zoom: 11,
    center: stl
  });
  geocoder = new google.maps.Geocoder();
  addresses = new Array()
}

//Drops a marker on the map for the given event's address. If a marker has already been placed
//for that address, does not drop a new marker.
function geocodeAddress(event){
    console.log("geocoding event: " + event.title);
    var allowGC = true;

    //only geocode an address once
    if(addresses.includes(event.address)){
      allowGC = false;
    } else {
      addresses.push(event.address);
    }

    if(allowGC){
      geocoder.geocode({'address': event.address}, function(results, status) {
        if (status === 'OK'){
          console.log("Dropping marker at " + results[0].geometry.location + " for address " + event.address);
          var marker = new google.maps.Marker({
            map: ourMap,
            position: results[0].geometry.location
          });
        } else {
            console.log("Could not find geolocation for address: " + event.address + " Status: " + status);
        }
      });
  } else {
    console.log(event.address + " already geocoded.");
  }
}



